import java.util.Scanner;

public class VirtualPetApp{
	public static void main (String [] args){
		Scanner reader = new Scanner(System.in);
		Panda embarrassment[] = new Panda[4];
		for(int i = 0; i < embarrassment.length; i++){
			embarrassment[i] = new Panda();
			System.out.println("What is the type of panda #" + (i+1) + " of the embarrassment?");
			embarrassment[i].type = reader.nextLine();
			System.out.println("Give the name of a panda from the movie 'Kung Fu Panda'?");
			embarrassment[i].name = reader.nextLine();
			System.out.println("What is the age of the panda");
			embarrassment[i].age = Integer.parseInt(reader.nextLine());
		}
		System.out.println("");
		System.out.println("Type: " +  embarrassment[3].type);
		System.out.println("Name: " +embarrassment[3].name);
		System.out.println("Age: " +embarrassment[3].age);
		System.out.println("");
		embarrassment[0].doFunActivity();
		embarrassment[0].isAMasterOfKungFu();
	}
}